import argparse

import mindpsore as ms
import mindspore.nn as nn
from mindspore import load_checkpoint, load_param_into_net, FixedLossScaleManager, Model, LossMonitor, TimeMonitor, \
    CheckpointConfig, ModelCheckpoint
from mindspore.communication import get_rank
from mindspore.dataset.transforms import TypeCast

from mission.models import create_model
from mission.data import create_dataset, Mixup, create_loader, transforms_imagenet_train, transforms_cifar10_train
from mission.loss import CrossEntropySmooth
from mission.optimizer import create_optimizer
from mission.scheduler import create_scheduler

ms.set_seed(1)


def train(args):
    ms.set_context(mode=ms.GRAPH_MODE, device_target=args.device_target)
    # create model
    model = create_model(model_name=args.model,
                         num_classes=args.num_classes,
                         drop_rate=args.drop_rate,
                         drop_path_rate=args.drop_path_rate,
                         pretrained=args.pretrained,
                         checkpoint_path=args.ckpt_pat)
    dataset_transform = model.dataset_transform

    # dataset
    dataset_train = create_dataset(
        name=args.dataset,
        root=args.data_dir,
        split=args.train_split,
        download=args.dataset_download,
        distribute=args.run_distribute)

    transform = None
    if args.dataset == 'imagenet':
        if 'transforms_imagenet_train' in dataset_transform:
            transform_param = dataset_transform['transforms_imagenet_train']
            transform = transforms_imagenet_train(**transform_param)
    elif args.dataset == 'cifar10':
        if 'transforms_cifar10_train' in dataset_transform:
            transform_param = dataset_transform['transforms_cifar10_train']
            transform = transforms_imagenet_train(**transform_param)

    transforms = None
    mixup_active = args.mixup_alpha > 0. or args.cutmix_alpha > 0. or args.cutmix_minmax is not None
    if mixup_active:
        mixup_args = dict(
            mixup_alpha=args.mixup_alpha, cutmix_alpha=args.cutmix_alpha, cutmix_minmax=args.cutmix_minmax,
            prob=args.mixup_prob, switch_prob=args.mixup_switch_prob, model=args.mixup_mode,
            label_smoothing=args.smoothing, num_classes=args.num_classes
        )
        transforms = Mixup(**mixup_args)

    target_transform = TypeCast(ms.int32)

    loader_train = create_loader(
        dataset=dataset_train,
        batch_size=args.batch_size,
        drop_remainder=args.drop_remainder,
        repeat_num=args.repeat_num,
        transform=transform,
        transform_input_columns=args.transform_input_columns,
        target_transform=target_transform,
        target_transform_input_columns=args.target_transform_input_columns,
        transforms=transforms,
        transforms_input_columns=args.transforms_input_columns,
        num_parallel_workers=args.num_parallel_workers,
    )
    batches_per_epoch = loader_train.get_dataset_size()

    # loss
    if args.mixup > 0.:
        pass # 补充 SoftTargetCrossEntropy损失函数
    elif args.smoothing:
        loss = CrossEntropySmooth(smooth_factor=args.smoothing,
                                  num_classes=args.num_classes)
    else:
        loss = nn.SoftmaxCrossEntropyWithLogits(sparse=True, reduction='mean')

    # learning rate schedule
    if args.lr_decay_mode is not None:
        lr = create_scheduler(lr_init=args.lr_init,
                              lr_end=args.lr_end,
                              lr_max=args.lr_max,
                              total_epochs=args.epoch_size,
                              warmup_epochs=args.warmup_epochs,
                              steps_per_epoch=batches_per_epoch,
                              lr_decay_mode=args.lr_decay_mode)
        args.lr = lr
    # optimizer
    optimizer = create_optimizer(args=args,
                                 model=model)

    # init model
    if args.loss_scale > 1.0:
        loss_scale_manager = FixedLossScaleManager(loss_scale=args.loss_scale, drop_overflow_update=False)
        model = Model(model, loss_fn=loss, optimizer=optimizer, metrics={'acc'}, amp_level=args.amp_level,
                      loss_scale_manager=loss_scale_manager)
    else:
        model = Model(model, loss_fn=loss, optimizer=optimizer, metrics={'acc'}, amp_level=args.amp_level)

    # callback
    loss_cb = LossMonitor(per_print_times=batches_per_epoch)
    time_cb = TimeMonitor(data_size=batches_per_epoch)
    callbacks = [loss_cb, time_cb]
    ckpt_config = CheckpointConfig(
        save_checkpoint_steps=batches_per_epoch,
        keep_checkpoint_max=args.keep_checkpoint_max)
    ckpt_cb = ModelCheckpoint(prefix=args.model,
                              directory=args.ckpt_save_dir,
                              config=ckpt_config)
    if args.run_distribute:
        if get_rank() == 0:
            callbacks.append(ckpt_cb)
    else:
        callbacks.append(ckpt_cb)

    # train model
    model.train(args.epoch_size, loader_train, callbacks=callbacks, dataset_sink_mode=args.data_sink_mode)

    if __name__ == '__main__':
        parser = argparse.ArgumentParser(description='Training.')
        parser.add_argument('--device_target', type=str, default="GPU", choices=["Ascend", "GPU", "CPU"],
                            help='Type of platform (default="GPU").')
        parser.add_argument('--run_distribute', type=bool, default=True, help='Run distribute (default=True).')
        parser.add_argument('--model', required=True, help='Name of model to train.')
        parser.add_argument('--num_classes', type=int, default=1000, help='Number of label classes (default=1000).')
        parser.add_argument('--pretrained', type=bool, default=False, help='Load pretrained model (default=False).')
        parser.add_argument('--ckpt_path', type=str, default='',
                            help='Initialize model from this checkpoint (default='').')
        parser.add_argument('--drop_rate', type=float, default=None, help='Drop rate (default=None).')
        parser.add_argument('--drop_path_rate', type=float, default=None, help='Drop path rate (default=None).')

        parser.add_argument('--dataset', type=str, default='image_folder',
                            help='Type of dataset (default="image_folder").')
        parser.add_argument('--data_dir', required=True, help='Path to dataset.')
        parser.add_argument('--train_split', type=str, default='train', help='Dataset train split (default="train").')
        parser.add_argument('--dataset_download', type=bool, default=False, help='Download dataset (default=False).')
        parser.add_argument('--mixup_alpha', type=float, default=0.,
                            help='Mixup alpha value, mixup is active if > 0 (default=0.).')
        parser.add_argument('--cutmix_alpha', type=float, default=0.,
                            help='Cutmix alpha value, cutmix is active if > 0 (default=0.).')
        parser.add_argument('--cutmix_mixmax', type=float, default=None,
                            help='Cutmix min/max ratio, overrides alpha and enables cutmix if set (default=None).')
        parser.add_argument('--mixup_prob', type=float, default=1.0,
                            help='Probability of performing mixup or cutmix when either/both is enabled (default=1.0).')
        parser.add_argument('--mixup_switch_prob', type=float, default=0.5,
                            help='Probability of switching to cutmix when both mixup and cutmix enabled (default=0.5).')
        parser.add_argument('--mixup_mode', type=str, default='batch',
                            help='How to apply mixup/cutmix params. Per "batch", "pair", or "elem" (default="batch").')
        parser.add_argument('--smoothing', type=float, default=0.1, help='Label smoothing (default=0.1).')
        parser.add_argument('--batch_size', type=int, default=64, help='Number of batch size (default=64).')
        parser.add_argument('--drop_remainder', type=bool, default=True,
                            help='Determines whether or not to drop the last block whose data '
                                 'row number is less than batch size (default=True).')
        parser.add_argument('--repeat_num', type=int, default=1, help='Number of repeat (default=1).')
        parser.add_argument('--num_parallel_workers', type=int, default=8,
                            help='Number of parallel workers (default=8).')
        parser.add_argument('--transform_input_columns', type=str, default="image",
                            help='List of the names of the columns that will be passed to the first'
                                 'operation as input (default="image").')
        parser.add_argument('--target_transform_input_columns', type=str, default="label",
                            help='List of the names of the columns that will be passed to the first'
                                 'operation as input (default="label").')
        parser.add_argument('--transforms_input_columns', type=list, default=["image", "label"],
                            help='List of the names of the columns that will be passed to the first'
                                 ' operation as input (default=["image", "label"]).')

        parser.add_argument('--opt', type=str, default='momentum', help='Optimizer (default="momentum").')
        parser.add_argument('--momentum', type=float, default=0.9, help='Optimizer momentum (default=0.9).')
        parser.add_argument('--weight_decay', type=float, default=0.0001, help='Weight decay (default=0.0001).')
        parser.add_argument('--loss_scale', type=float, default=1.0, help='Loss scale (default=1.0).')
        parser.add_argument('--opt_eps', type=float, default=1e-8, help='Optimizer epsilon (default=1e-8).')
        parser.add_argument('--lr', type=float, default=0.01, help='Learning rate (default=0.01).')

        parser.add_argument('--amp_level', type=str, default='O0', help='Amp level (default="O0").')
        parser.add_argument('--keep_checkpoint_max', type=int, default=10,
                            help='Max number of checkpoint files (default=10).')
        parser.add_argument('--ckpt_save_dir', type=str, default="./ckpt", help='Path of ckpt (default="./ckpt").')
        parser.add_argument('--epoch_size', type=int, default=90, help='Train epoch size (default=90).')
        parser.add_argument('--dataset_sink_mode', type=bool, default=True,
                            help='The dataset sink mode (default=True).')

        parser.add_argument('--lr_init', type=float, default=0., help='Init learning rate (default=0.).')
        parser.add_argument('--lr_end', type=float, default=0., help='End learning rate (default=0.).')
        parser.add_argument('--lr_max', type=float, default=0.1, help='Max learning rate (default=0.1).')
        parser.add_argument('--lr_decay_mode', type=str, default=None, help='Decay mode of lr (default=None).')
        parser.add_argument('--warmup_epochs', type=int, default=0, help='Warmup epochs (default=0).')

        args = parser.parse_known_args()[0]
        train(args)
