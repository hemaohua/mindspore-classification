import mindspore as ms
from .cosine_lr import cosine_lr_scheduler


def create_scheduler(lr_init, lr_end, lr_max, total_epochs, warmup_epochs, steps_per_epoch, lr_decay_mode):
    total_steps = steps_per_epoch * total_epochs
    warmup_steps = steps_per_epoch * warmup_epochs

    if lr_decay_mode == 'cosine':
        lr_each_step = cosine_lr_scheduler(lr_init, lr_end, lr_max, total_steps, warmup_steps)

    return ms.Tensor(lr_each_step, dtype=ms.float32)


