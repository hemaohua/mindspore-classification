# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Dataset Download"""

# Copyright 2021 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
""" Create the MNIST dataset. """

import os
from ..utils.download import DownLoad


class MnistDownload:
    url_path = 'http://yann.lecun.com/exdb/mnist/'

    resources = {"train": [("train-images-idx3-ubyte.gz", "f68b3c2dcbeaaa9fbdd348bbdeb94873"),
                           ("train-labels-idx1-ubyte.gz", "d53e105ee54ea40749a09fcbcd1e9432")],
                 "test": [("t10k-images-idx3-ubyte.gz", "9fb629c4189551a2d022fa330f9573f3"),
                          ("t10k-labels-idx1-ubyte.gz", "ec29112dd5afa0611ce80d1b7f02629c")]}

    def __init__(self, root: str, split: str):
        self.root = root
        self.split = split
        self.path = os.path.join(self.root, self.split)

    def download_and_extract_archive(self):
        """Download the MNIST dataset if it doesn't exists."""
        bool_list = []
        # Check whether the file exists and check value of md5.
        for url, _ in self.resources[self.split]:
            filename = os.path.splitext(url)[0]
            file_path = os.path.join(self.path, filename)
            bool_list.append(os.path.isfile(file_path))
        if all(bool_list):
            return

        # download files
        for filename, md5 in self.resources[self.split]:
            url = os.path.join(self.url_path, filename)
            DownLoad().download_and_extract_archive(url,
                                                    download_path=self.path,
                                                    filename=filename,
                                                    md5=md5)


class FashionMnistDownload:
    pass


class Cifar10Download:
    pass


class Cifar100Download:
    pass
