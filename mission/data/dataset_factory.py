# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Dataset Factory"""

import os

from .dataset_download import MnistDownload, FashionMnistDownload, Cifar10Download, Cifar100Download

import mindspore as ms
from mindspore.communication import init, get_rank, get_group_size
from mindspore.dataset import MnistDataset, KMnistDataset, FashionMnistDataset, Cifar10Dataset, Cifar100Dataset, \
    ImageFolderDataset

_MINDSPORE_BASIC_DATASET = dict(
    mnist=(MnistDataset, MnistDownload),
    fashion_mnist=(FashionMnistDataset, FashionMnistDownload),
    cifar10=(Cifar10Dataset, Cifar10Download),
    cifar100=(Cifar100Dataset, Cifar100Download)
)


def create_dataset(
        name,
        root,
        split: str = 'train',
        search_split: bool = True,
        download: bool = False,
        distribute: bool = False,
        **kwargs
):
    name = name.lower()
    if name in _MINDSPORE_BASIC_DATASET:
        dataset_class = _MINDSPORE_BASIC_DATASET[name][0]
        dataset_download = _MINDSPORE_BASIC_DATASET[name][1]
        if download and dataset_download:
            dataset_download(root, split).download_and_extract_archive()
        else:
            raise ValueError("Dataset download is not supported.")
        if distribute:
            init()
            device_num = get_group_size()
            rank_id = get_rank()
            ms.set_auto_parallel_context(device_num=device_num,
                                         parallel_mode='data_parallel',
                                         gradients_mean=True)
            mindspore_kwargs = dict(dataset_dir=root, num_shards=device_num, shard_id=rank_id, **kwargs)
        else:
            mindspore_kwargs = dict(dataset_dir=root, **kwargs)
        dataset = dataset_class(usage=split, **mindspore_kwargs)
    elif name == 'image_folder' or name == 'folder' or name == 'imagenet':
        if download:
            raise ValueError("Dataset download is not supported.")
        if search_split and os.path.isdir(root):
            root = os.path.join(root, split)
        dataset = ImageFolderDataset(root, **kwargs)
    else:
        assert False, "Unknown dataset"

    return dataset
