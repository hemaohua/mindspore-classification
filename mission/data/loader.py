# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Dataset Loader"""


def create_loader(
        dataset,
        batch_size,
        drop_remainder=False,
        repeat_num=1,
        transform=None,
        transform_input_columns="image",
        transform_output_columns=None,
        transform_column_order=None,
        target_transform=None,
        target_transform_input_columns="label",
        target_transform_output_columns=None,
        target_transform_column_order=None,
        transforms=None,
        transforms_input_columns=None,
        transforms_output_columns=None,
        transforms_column_order=None,
        num_parallel_workers=None,
        python_multiprocessing=False,
):
    if transform:
        dataset.map(operations=transform,
                    input_columns=transform_input_columns,
                    output_columns=transform_output_columns,
                    column_order=transform_column_order,
                    num_parallel_workers=num_parallel_workers,
                    python_multiprocessing=python_multiprocessing)

    if target_transform:
        dataset.map(operations=transform,
                    input_columns=target_transform_input_columns,
                    output_columns=target_transform_output_columns,
                    column_order=target_transform_column_order,
                    num_parallel_workers=num_parallel_workers,
                    python_multiprocessing=python_multiprocessing)
    if transforms:
        dataset.map(operations=transforms,
                    input_columns=transforms_input_columns,
                    output_columns=transforms_output_columns,
                    column_order=transforms_column_order,
                    num_parallel_workers=num_parallel_workers,
                    python_multiprocessing=python_multiprocessing)
    dataset.batch(batch_size=batch_size, drop_remainder=drop_remainder)
    dataset.repeat(repeat_num=repeat_num)

    return dataset
