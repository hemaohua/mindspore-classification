# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Transforms Factory"""
import math

import mindpsore.dataset.vision as transforms
from mindspore.dataset.vision import Inter

from .constants import DEFAULT_CROP_PCT, IMAGENET_DEFAULT_MEAN, IMAGENET_DEFAULT_STD
from .auto_augment import rand_augment_transform, augment_and_mix_transform, auto_augment_transform, _pil_interp
from .random_erasing import RandomErasing


def transforms_imagenet_train(
        image_resize=224,
        scale=(0.08, 1.0),
        ratio=(0.75, 1.333),
        hflip=0.5,
        vflip=0.0,
        color_jitter=0.4,
        auto_augment=None,
        interpolation='bilinear',
        mean=IMAGENET_DEFAULT_MEAN,
        std=IMAGENET_DEFAULT_STD,
        re_prob=0.,
        re_mode='const',
        re_count=1,
        re_num_splits=0
):
    # Define map operations for training dataset
    if hasattr(Inter, interpolation.upper()):
        interpolation = getattr(Inter, interpolation.upper())
    else:
        interpolation = Inter.BILINEAR

    trans_list = [transforms.RandomCropDecodeResize(size=image_resize,
                                                    scale=scale,
                                                    ratio=ratio,
                                                    interpolation=interpolation
                                                    )]
    if hflip > 0.:
        trans_list += [transforms.RandomHorizontalFlip(prob=hflip)]
    if vflip > 0.:
        trans_list += [transforms.RandomVerticalFlip(prob=vflip)]

    if auto_augment:
        assert isinstance(auto_augment, str)
        trans_list += [transforms.ToPIL()]
        if isinstance(image_resize, (tuple, list)):
            img_size_min = min(image_resize)
        else:
            img_size_min = image_resize
        aa_params = dict(
            translate_const=int(img_size_min * 0.45),
            img_mean=tuple([min(255, round(x)) for x in mean]),
        )
        aa_params['interpolation'] = _pil_interp(interpolation)
        if auto_augment.startswith('rand'):
            trans_list += [rand_augment_transform(auto_augment, aa_params)]
        elif auto_augment.startswith('augmix'):
            aa_params['translate_pct'] = 0.3
            trans_list += [augment_and_mix_transform(auto_augment, aa_params)]
        else:
            trans_list += [auto_augment_transform(auto_augment, aa_params)]
        trans_list += [transforms.ToTensor()]
    elif color_jitter is not None:
        if isinstance(color_jitter, (list, tuple)):
            # color jitter shoulf be a 3-tuple/list for brightness/contrast/saturation
            # or 4 if also augmenting hue
            assert len(color_jitter) in (3, 4)
        else:
            color_jitter = (float(color_jitter),) * 3
        trans_list += [transforms.RandomColorAdjust(*color_jitter)]

    trans_list += [
        transforms.Normalize(mean=mean, std=std),
        transforms.HWC2CHW()
    ]
    if re_prob > 0.:
        trans_list.append(
            RandomErasing(re_prob, mode=re_mode, max_count=re_count, num_splits=re_num_splits)
        )

    return trans_list


def transforms_imagenet_eval(
        image_resize=224,
        crop_pct=DEFAULT_CROP_PCT,
        mean=IMAGENET_DEFAULT_MEAN,
        std=IMAGENET_DEFAULT_STD,
        interpolation='bilinear',

):
    if isinstance(image_resize, (tuple, list)):
        assert len(image_resize) == 2
        if image_resize[-1] == image_resize[-2]:
            scale_size = int(math.floor(image_resize[0] / crop_pct))
        else:
            scale_size = tuple([int(x / crop_pct) for x in image_resize])
    else:
        scale_size = int(math.floor(image_resize / crop_pct))

    # Define map operations for training dataset
    if hasattr(Inter, interpolation.upper()):
        interpolation = getattr(Inter, interpolation.upper())
    else:
        interpolation = Inter.BILINEAR
    trans_list = [
        transforms.Decode(),
        transforms.Resize(scale_size, interpolation=interpolation),
        transforms.CenterCrop(image_resize),
        transforms.Normalize(mean=mean, std=std),
        transforms.HWC2CHW()

    ]

    return trans_list


def transforms_cifar10_train():
    pass


def transforms_cifar10_eval():
    pass


def transforms_mnsit_train():
    pass


def transforms_mnsit_eval():
    pass
