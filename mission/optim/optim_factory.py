import mindspore.nn as nn


def init_group_params(model, weight_decay):
    decay_params = []
    no_decay_params = []

    for param in model.trainable_params():
        if 'beta' not in param.name and 'gamma' not in param.name and 'bias' not in param.name:
            decay_params.append(param)
        else:
            no_decay_params.append(param)
    return [
        {'params': decay_params, 'weight_decay': weight_decay},
        {'params': no_decay_params},
        {'order_params': model.trainable_params()}
    ]


def create_optimizer(args, model, filter_bias_and_bn=True):
    weight_decay = args.weight_decay
    if weight_decay and filter_bias_and_bn:
        params = init_group_params(model, weight_decay)
    else:
        params = model.trainable_params()

    if args.opt.lower() == 'momentum':
        optimizer = nn.Momentum(params=params,
                                learning_rate=args.lr,
                                momentum=args.momentum,
                                weight_decay=weight_decay,
                                loss_scale=args.loss_scale)
    elif args.opt.lower() == 'rmsprop':
        optimizer = nn.RMSProp(params=params,
                               learning_rate=args.lr,
                               momentum=args.momentum,
                               weight_decay=weight_decay,
                               loss_scale=args.loss_scale,
                               epsilon=args.opt_eps)
    elif args.opt.lower() == 'sgd':
        optimizer = nn.SGD(params=params,
                           learning_rate=args.lr,
                           momentum=args.momentum,
                           weight_decay=weight_decay,
                           loss_scale=args.loss_scale)
    else:
        raise ValueError('Invalid optimizer.')

    return optimizer
