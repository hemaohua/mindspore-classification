# Copyright 2021 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""GENet."""
import math
import mindspore.nn as nn
from mindspore.ops import operations as P
from mindspore.common import initializer as init
from mindspore.common.initializer import HeNormal
from .registry import register_model

__all__ = ["ge_resnet50"]


# 删除无用的函数块，如conv1x1、conv3x3等
# 用HeNormal替代 自定义的kaiming_normal
# 使用init.initializer初始化权重
def conv7x7(in_channel, out_channel, stride=1):
    weight_shape = (out_channel, in_channel, 7, 7)
    weight = init.initializer(HeNormal(mode="fan_out", nonlinearity="relu"),
                              weight_shape, mindspore.float32)
    return nn.Conv2d(in_channel, out_channel,
                     kernel_size=7, stride=stride, padding=0, pad_mode='same', weight_init=weight)


def bn(channel):
    return nn.BatchNorm2d(channel, eps=1e-4, momentum=0.95,
                          gamma_init=1, beta_init=0, moving_mean_init=0, moving_var_init=1)


def fc(in_channel, out_channel):
    weight_shape = (out_channel, in_channel)
    weight = init.initializer(HeNormal(math.sqrt(5)), weight_shape, mindspore.float32)
    return nn.Dense(in_channel, out_channel, has_bias=True, weight_init=weight, bias_init=0)


class GEBlock(nn.Cell):
    """
    Args:
        in_channel (int): Input channel.
        out_channel (int): Output channel.
        stride (int): Stride size for the first convolutional layer. Default: 1.
        spatial(int) : output_size of block
        extra_params(bool)  : Whether to use DW Conv to down-sample
        mlp(bool)      : Whether to combine SENet (using 1*1 conv)
    Returns:
        Tensor, output tensor.
    Examples:
        >>> GEBlock(3, 128, 2, 56, True, True)
    """

    def __init__(self, in_channel, out_channel, stride, spatial, extra_params, mlp):
        super().__init__()
        expansion = 4

        self.mlp = mlp
        self.extra_params = extra_params

        # middle channel num
        channel = out_channel // expansion
        # nn.Conv2dBnAct官网没有文档说明
        self.conv1 = nn.Conv2dBnAct(in_channel, channel, kernel_size=1, stride=1,
                                    has_bn=True, pad_mode="same", activation='relu')

        self.conv2 = nn.Conv2dBnAct(channel, channel, kernel_size=3, stride=stride,
                                    has_bn=True, pad_mode="same", activation='relu')

        self.conv3 = nn.Conv2dBnAct(channel, out_channel, kernel_size=1, stride=1, pad_mode='same',
                                    has_bn=True)

        # whether down-sample identity
        self.down_sample = (stride != 1 or in_channel != out_channel)
        if self.down_sample:
            self.down_layer = nn.Conv2dBnAct(in_channel, out_channel,
                                             kernel_size=1, stride=stride,
                                             pad_mode='same', has_bn=True)

        if extra_params:
            cellList = []
            # implementation of DW Conv has some bug while kernel_size is too big, so down sample
            if spatial >= 56:
                cellList.extend([nn.Conv2d(in_channels=out_channel,
                                           out_channels=out_channel,
                                           kernel_size=3,
                                           stride=2,
                                           pad_mode="same"),
                                 nn.BatchNorm2d(out_channel)])
                spatial //= 2

            cellList.extend([nn.Conv2d(in_channels=out_channel,
                                       out_channels=out_channel,
                                       kernel_size=spatial,
                                       group=out_channel,
                                       stride=1,
                                       padding=0,
                                       pad_mode="pad"),
                             nn.BatchNorm2d(out_channel)])

            self.downop = nn.SequentialCell(cellList)

        else:

            self.downop = P.ReduceMean(keep_dims=True)

        if mlp:
            mlpLayer = []
            mlpLayer.append(nn.Conv2d(in_channels=out_channel,
                                      out_channels=out_channel//16,
                                      kernel_size=1))
            mlpLayer.append(nn.ReLU())
            mlpLayer.append(nn.Conv2d(in_channels=out_channel//16,
                                      out_channels=out_channel,
                                      kernel_size=1))
            self.mlpLayer = nn.SequentialCell(mlpLayer)

        self.sigmoid = nn.Sigmoid()
        self.add = P.Add()
        self.relu = nn.ReLU()
        self.mul = P.Mul()

    def construct(self, x):
        identity = x
        out = self.conv1(x)
        out = self.conv2(out)
        out = self.conv3(out)

        if self.down_sample:
            identity = self.down_layer(identity)

        if self.extra_params:
            out_ge = self.downop(out)
        else:
            out_ge = self.downop(out, (2, 3))

        if self.mlp:
            out_ge = self.mlpLayer(out_ge)
        out_ge = self.sigmoid(out_ge)
        out = self.mul(out, out_ge)
        out = self.add(out, identity)
        out = self.relu(out)

        return out


class GENet(nn.Cell):
    """
    Gather-Excite: Exploiting Feature Context in Convolutional Neural Networks
    https://arxiv.org/abs/1810.12348
    GENet architecture.

    Args:
        block (Cell): Block for network.
        layer_nums (list): Numbers of block in different layers.
        in_channels (list): Input channel in each layer.
        out_channels (list): Output channel in each layer.
        strides (list):  Stride size in each layer.
        spatial(list):   Numbers of output spatial size of different groups.
        num_classes (int): The number of classes that the training images are belonging to.
        extra_params(bool)    : Whether to use DW Conv to down-sample
        mlp(bool)      : Whether to combine SENet (using 1*1 conv)

    Returns:
        Tensor, output tensor.

    Examples:
        >>> GENet(GEBlock,
        >>>        [3, 4, 6, 3],
        >>>        [64, 256, 512, 1024],
        >>>        [256, 512, 1024, 2048],
        >>>        [1, 2, 2, 2],
        >>>        [56,28,14,7]
        >>>        1001,True,True)
    """

    def __init__(self, block, layer_nums, in_channels, out_channels, strides, spatial, num_classes,
                 extra_params, mlp):
        super(GENet, self).__init__()

        if not len(layer_nums) == len(in_channels) == len(out_channels) == 4:
            raise ValueError("the length of layer_num, in_channels, out_channels list must be 4!")
        self.extra = extra_params

        # initial stage
        self.conv1 = conv7x7(3, 64, stride=2)
        self.bn1 = bn(64)
        self.relu = P.ReLU()
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, pad_mode="same")

        # 用循环封装4个layer，之前是一个一个写的
        self.layers = []
        for i in range(len(layer_nums)):
            self.layers.append(self._make_layer(block=block,
                                                layer_num=layer_nums[i],
                                                in_channel=in_channels[i],
                                                out_channel=out_channels[i],
                                                stride=strides[i],
                                                spatial=spatial[i],
                                                extra_params=extra_params,
                                                mlp=mlp))
        self.layers = nn.SequentialCell(self.layers)

        self.mean = P.ReduceMean(keep_dims=True)
        self.flatten = nn.Flatten()
        self.end_point = fc(out_channels[3], num_classes)

    def _make_layer(self, block, layer_num, in_channel, out_channel,
                    stride, spatial, extra_params, mlp):
        """
        Make stage network of GENet.
        """
        layers = []

        ge_block = block(in_channel=in_channel,
                         out_channel=out_channel,
                         stride=stride,
                         spatial=spatial,
                         extra_params=extra_params,
                         mlp=mlp)
        layers.append(ge_block)
        for _ in range(1, layer_num):
            ge_block = block(in_channel=out_channel,
                             out_channel=out_channel,
                             stride=1,
                             spatial=spatial,
                             extra_params=extra_params,
                             mlp=mlp)
            layers.append(ge_block)
        return nn.SequentialCell(layers)

    def construct(self, x):
        # initial stage
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)

        # four groups
        c = self.layers(x)

        out = self.mean(c, (2, 3))
        out = self.flatten(out)
        out = self.end_point(out)

        return out


@register_model
def ge_resnet50(class_num=1000, extra=True, mlp=True, pretrained=False):
    net = GENet(block=GEBlock,
                layer_nums=[3, 4, 6, 3],
                in_channels=[64, 256, 512, 1024],
                out_channels=[256, 512, 1024, 2048],
                strides=[1, 2, 2, 2],
                spatial=[56, 28, 14, 7],
                num_classes=class_num,
                extra_params=extra,
                mlp=mlp)
    if pretrained:
        pass
    return net


if __name__ == '__main__':
    import mindspore
    from mindspore import Tensor, context
    import numpy as np
    context.set_context(mode=context.PYNATIVE_MODE)
    input = Tensor(np.random.random((1, 3, 224, 224)), mindspore.float32)
    genet = ge_resnet50()
    out = genet(input)
    print(out.shape)