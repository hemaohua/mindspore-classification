# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""
Autoslim resnet backbone
"""
from mindspore import nn
from .registry import register_model
# SlimmableLinear, SwitchableBatchNorm2d, SlimmableConv2d
# 分别对Dense，BatchNorm2d，Conv2d进行了封装，没有做任何操作，有无必要？
# 可能只是想与pytorch模型结构保持一致

__all__ = ['autoslim']


class SwitchableBatchNorm2d(nn.Cell):
    def __init__(self, num_features_list):
        super(SwitchableBatchNorm2d, self).__init__()
        self.bn = nn.BatchNorm2d(num_features_list[1])

    def construct(self, input0):
        return self.bn(input0)


class SlimmableConv2d(nn.Conv2d):
    def __init__(self, in_channels_list,
                 out_channels_list,
                 kernel_size,
                 stride=1,
                 padding=0,
                 dilation=1,
                 bias=True):
        super(SlimmableConv2d, self).__init__(in_channels_list[1],
                                              out_channels_list[1],
                                              kernel_size,
                                              stride=stride,
                                              pad_mode='pad',
                                              padding=padding,
                                              dilation=dilation,
                                              group=1,
                                              has_bias=bias)


class SlimmableLinear(nn.Dense):
    def __init__(self,
                 in_features_list,
                 out_features_list,
                 bias=True):
        super(SlimmableLinear, self).__init__(in_features_list[1],
                                              out_features_list[1],
                                              has_bias=bias)


def pop_channels(autoslim_channels):
    return [i.pop(0) for i in autoslim_channels]


class Block(nn.Cell):
    def __init__(self, inp, outp, midp1, midp2, stride):
        super(Block, self).__init__()
        assert stride in [1, 2]

        # layers封装成三个nn.Conv2dBnAct?
        layers = [
            SlimmableConv2d(inp, midp1, 1, 1, 0, bias=False),
            SwitchableBatchNorm2d(midp1),
            nn.ReLU(),

            SlimmableConv2d(midp1, midp2, 3, stride, 1, bias=False),
            SwitchableBatchNorm2d(midp2),
            nn.ReLU(),

            SlimmableConv2d(midp2, outp, 1, 1, 0, bias=False),
            SwitchableBatchNorm2d(outp),
        ]
        self.body = nn.SequentialCell(layers)

        self.residual_connection = (stride == 1 and inp == outp)
        if not self.residual_connection:
            # 用nn.Conv2dBnAct代替？
            self.shortcut = nn.SequentialCell(
                SlimmableConv2d(inp, outp, 1, stride=stride, bias=False),
                SwitchableBatchNorm2d(outp),
            )
        self.post_relu = nn.ReLU()

    def construct(self, x):
        res = self.body(x)
        if self.residual_connection:
            res += x
        else:
            res += self.shortcut(x)
        res = self.post_relu(res)
        return res


# 将yaml文件中的三个参数reset_parameters， depth， channel_num_list提取出来，初始化到网络当中
# 添加**kwargs用来传递这三个参数
# 删除数据预处理，之前是将数据预处理放在网络当中了，说是可以提高效率
class AutoSlimModel(nn.Cell):
    """
    AutoSlim: Towards One-Shot Architecture Search for Channel Numbers
    https://arxiv.org/abs/1903.11728

    Args:
        num_classes(int): class number. Default 1000.
        input_size(int):
    """
    def __init__(self, num_classes=1000, input_size=224, **kwargs):
        super(AutoSlimModel, self).__init__()
        reset_parameters = True if 'reset_parameters' not in kwargs else kwargs['reset_parameters'],
        depth = 50 if 'depth' not in kwargs else kwargs['depth']
        default_channel_num_list = [
            [24, 72, 8, 16, 72, 16, 16, 72, 8, 8, 72, 240, 16, 24, 240, 16, 24, 240, 16, 16, 240, 16, 24, 240, 704, 72,
             72, 704, 48, 72, 704, 48, 72, 704, 72, 72, 704, 48, 48, 704, 48, 48, 704, 1680,280, 240, 1680, 120, 200,
             1680, 280, 280, 1680, 1000],
            [32, 72, 16, 16, 72, 16, 16, 72, 8, 8, 72, 280, 24, 40, 280, 16, 24, 280, 16, 24, 280, 24, 40, 280, 880, 72,
             72, 880, 72, 96, 880, 72, 72, 880, 72, 72, 880, 48, 72, 880, 168, 96, 8890, 2016, 360, 280, 2016, 320, 360,
             2016, 480, 440, 2016, 1000],
            [40, 120, 16, 16, 120, 24, 24, 120, 16, 24, 120, 440, 48, 72, 440, 32, 48, 440, 40, 56, 440, 64, 72, 440,
             1024, 168, 168, 1024, 96, 144, 1024, 120, 168, 1024, 120, 144, 1024, 120, 144, 1024, 256, 256, 1024, 2016,
             512, 512, 2016, 480, 480, 2016, 512, 512, 2016, 1000],
            [48, 192, 16, 16, 192, 56, 56, 192, 24, 40, 192, 512, 88, 112, 512, 64, 104, 512, 104, 112, 512, 80, 128,
             512, 1024, 240, 216, 1024, 144, 216, 1024, 192, 192, 1024, 144, 240, 1024, 240, 256, 1024, 256, 256, 1024,
             2016, 512, 512, 2016, 480, 512, 2016, 512, 512, 2016, 1000]
        ]
        if 'channel_num_list' in kwargs:
            default_channel_num_list = kwargs['channel_num_list']
        self.features = []
        assert input_size % 32 == 0

        # setting of inverted residual blocks
        self.block_setting_dict = {
            # : [stage1, stage2, stage3, stage4]
            50: [3, 4, 6, 3],
            101: [3, 4, 23, 3],
            152: [3, 8, 36, 3],
        }
        self.block_setting = self.block_setting_dict[depth]
        # feats = [64, 128, 256, 512]
        channels = pop_channels(default_channel_num_list)
        self.features.append(
            nn.SequentialCell(
                SlimmableConv2d(
                    [3 for _ in range(len(channels))], # [3,3,3,3]
                    channels, 7, 2, 3, bias=False),
                SwitchableBatchNorm2d(channels),
                nn.ReLU(),
                nn.MaxPool2d(3, 2, 'same'),
            )
        )

        # body
        for stage_id, n in enumerate(self.block_setting):
            for i in range(n):
                if i == 0:
                    outp = pop_channels(default_channel_num_list)
                midp1 = pop_channels(default_channel_num_list)
                midp2 = pop_channels(default_channel_num_list)
                outp = pop_channels(default_channel_num_list)
                if i == 0 and stage_id != 0:
                    self.features.append(
                        Block(channels, outp, midp1, midp2, 2))
                else:
                    self.features.append(
                        Block(channels, outp, midp1, midp2, 1))
                channels = outp

        # cifar10
        avg_pool_size = input_size // 32
        self.features.append(nn.AvgPool2d(avg_pool_size))

        # make it nn.Sequential
        self.features = nn.SequentialCell(self.features)

        # classifier
        self.outp = channels
        self.classifier = SlimmableLinear(
            self.outp,
            [num_classes for _ in range(len(self.outp))])

        if reset_parameters:
            self.init_parameters_data()

    def construct(self, x):
        x = self.features(x)
        last_dim = x.shape[1]
        x = x.view(-1, last_dim)
        return self.classifier(x)

# 添加ckpt_url
cfg = {
    'autoclim': ""
}

# 添加autoslim接口
@register_model
def autoslim(num_classes=1000, input_size=224, pretrained=False, **kwargs):
    net = AutoSlimModel(num_classes=num_classes, input_size=input_size, **kwargs)
    if pretrained:
        pass
    return net


if __name__ == '__main__':
    import mindspore
    from mindspore import context, Tensor
    import numpy as np
    context.set_context(mode=context.PYNATIVE_MODE)
    input = Tensor(np.random.random((1, 3, 224, 224)), mindspore.float32)
    autos = autoslim()
    out = autos(input)
    print(out.shape)
