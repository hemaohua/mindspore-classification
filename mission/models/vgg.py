        # Copyright 2020 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""
MindSpore implementation of `VGG`.
Refer to Very Deep Convolutional Networks for Large-Scale Image Recognition.
"""
import math
import mindspore.nn as nn
import mindspore.common.dtype as mstype
from mindspore.common.initializer import initializer, HeNormal
from .registry import register_model

__all__ = ['Vgg', 'vgg11', 'vgg11_bn', 'vgg13', 'vgg13_bn', 'vgg16', 'vgg16_bn',
           'vgg19', 'vgg19_bn']


def _make_layer(base, args, has_bn):
    layers = []
    in_channels = 3
    for v in base:
        if v == 'M':
            layers += [nn.MaxPool2d(kernel_size=2, stride=2)]
        else:
            weight = 'ones'
            if args["initialize_mode"] == "XavierUniform":
                weight_shape = (v, in_channels, 3, 3)
                weight = initializer('XavierUniform', shape=weight_shape, dtype=mstype.float32)

            conv2d = nn.Conv2d(in_channels=in_channels,
                               out_channels=v,
                               kernel_size=3,
                               padding=args["padding"],
                               pad_mode=args["pad_mode"],
                               has_bias=args["has_bias"],
                               weight_init=weight)
            if has_bn:
                layers += [conv2d, nn.BatchNorm2d(v), nn.ReLU()]
            else:
                layers += [conv2d, nn.ReLU()]
            in_channels = v
    return nn.SequentialCell(layers)


class Vgg(nn.Cell):
    """
    Very Deep Convolutional Networks for Large-Scale Image Recognition
    https://arxiv.org/abs/1409.1556
    VGG network definition.

    Args:
        base(list): Configuration for different layers, mainly the channel number of Conv layer.
        args(dict): Some parameters of the network. Default None.
        num_classes(int): Class numbers. Default: 1000.
        drop_rate(float): probability of element to be zeroed.
        has_bn(bool): Whether to add batchnorm to the ConvBnAct.
        include_top(bool): Whether to include the 3 fully-connected layers at the top of the network. Default: True.
    """

    # 添加drop_rate参数，用于配置drop_rate，原drop_rate是固定的0.5
    # 删除batch_size参数，暂时没有用到
    # 删除phase参数，用来区分train和test，当等于test时，dropout的drop_rate为0，Dropout的表现在train和test是不同的
    # 这是由框架控制的，不需要手动设置
    def __init__(self, base, num_classes=1000, args=None, drop_rate=0.5, has_bn=False, include_top=True):
        super(Vgg, self).__init__()
        # 添加默认参数，如果传入args就更新default_args
        default_args = {
            "initialize_mode": "KaimingNormal",
            "pad_mode": "pad",
            "padding": 1,
            "has_bias": False,
        }
        if isinstance(args, dict):
            default_args.update(args)

        self.layers = _make_layer(base, default_args, has_bn)
        self.include_top = include_top
        self.flatten = nn.Flatten()
        # 与torchvision相比在flatten之前缺少AdaptiveAvgPool2d((7, 7))
        self.classifier = nn.SequentialCell([
            nn.Dense(512 * 7 * 7, 4096),
            nn.ReLU(),
            nn.Dropout(1 - drop_rate),
            nn.Dense(4096, 4096),
            nn.ReLU(),
            nn.Dropout(1 - drop_rate),
            nn.Dense(4096, num_classes)])
        if default_args["initialize_mode"] == "KaimingNormal":
            # 为啥会有两个初始化权重
            # 删除default_recurisive_init(self)
            self._init_weights()

    def construct(self, x):
        x = self.layers(x)
        if self.include_top:
            x = self.flatten(x)
            x = self.classifier(x)
        return x

    # 用HeNormal代替自定义的kaiming_normal
    # 与torch对其，添加对BatchNormal2d的权重初始化
    def _init_weights(self):
        for _, cell in self.cells_and_names():
            if isinstance(cell, nn.Conv2d):
                cell.weight.set_data(initializer(
                    HeNormal(math.sqrt(5), mode='fan_out', nonlinearity='relu'),
                    cell.weight.shape, cell.weight.dtype))
                if cell.bias is not None:
                    cell.bias.set_data(initializer('zeros', cell.bias.shape, cell.bias.dtype))
            elif isinstance(cell, nn.Dense):
                cell.weight.set_data(initializer('normal', cell.weight.shape, cell.weight.dtype))
                if cell.bias is not None:
                    cell.bias.set_data(initializer('zeros', cell.bias.shape, cell.bias.dtype))
            elif isinstance(cell, nn.BatchNorm2d):
                cell.weight.set_data(initializer('ones', cell.weight.shape, cell.weight.dtype))
                if cell.bias is not None:
                    cell.bias.set_data(initializer('zeros', cell.bias.shape, cell.bias.dtype))


bases = {
    '11': [64, 'M', 128, 'M', 256, 256, 'M', 512, 512, 'M', 512, 512, 'M'],
    '13': [64, 64, 'M', 128, 128, 'M', 256, 256, 'M', 512, 512, 'M', 512, 512, 'M'],
    '16': [64, 64, 'M', 128, 128, 'M', 256, 256, 256, 'M', 512, 512, 512, 'M', 512, 512, 512, 'M'],
    '19': [64, 64, 'M', 128, 128, 'M', 256, 256, 256, 256, 'M', 512, 512, 512, 512, 'M', 512, 512, 512, 512, 'M'],
}

ckpt_args = {
    'vgg11': '',
    'vgg11_bn': '',
    'vgg13': '',
    'vgg13_bn': '',
    'vgg16': '',
    'vgg16_bn': '',
    'vgg19': '',
    'vgg19_bn': '',
}


# 如果是Cifar10，args配置如下
# args = {
#     "initialize_mode": "XavierUniform",
#     "pad_mode": "same",
#     "padding": 0,
#     "has_bias": False,
# }

# 添加bn接口
@register_model
def vgg11(num_classes=1000, args=None, pretrained=False, **kwargs):
    net = Vgg(bases['11'], num_classes=num_classes, args=args, **kwargs)
    if pretrained:
        pass
    return net


@register_model
def vgg11_bn(num_classes=1000, args=None, pretrained=False, **kwargs):
    net = Vgg(bases['11'], num_classes=num_classes, args=args, has_bn=True, **kwargs)
    if pretrained:
        pass
    return net


@register_model
def vgg13(num_classes=1000, args=None, pretrained=False, **kwargs):
    net = Vgg(bases['13'], num_classes=num_classes, args=args, **kwargs)
    if pretrained:
        pass
    return net


@register_model
def vgg13_bn(num_classes=1000, args=None, pretrained=False, **kwargs):
    net = Vgg(bases['13'], num_classes=num_classes, args=args, has_bn=True, **kwargs)
    if pretrained:
        pass
    return net


@register_model
def vgg16(num_classes=1000, args=None, pretrained=False, **kwargs):
    net = Vgg(bases['16'], num_classes=num_classes, args=args, **kwargs)
    if pretrained:
        pass
    return net


@register_model
def vgg16_bn(num_classes=1000, args=None, pretrained=False, **kwargs):
    net = Vgg(bases['16'], num_classes=num_classes, args=args, has_bn=True, **kwargs)
    if pretrained:
        pass
    return net


@register_model
def vgg19(num_classes=1000, args=None, pretrained=False, **kwargs):
    net = Vgg(bases['19'], num_classes=num_classes, args=args, **kwargs)
    if pretrained:
        pass
    return net


@register_model
def vgg19_bn(num_classes=1000, args=None, pretrained=False, **kwargs):
    net = Vgg(bases['19'], num_classes=num_classes, args=args, has_bn=True, **kwargs)
    if pretrained:
        pass
    return net


if __name__ == '__main__':
    import mindspore
    from mindspore import context, Tensor
    import numpy as np
    context.set_context(mode=context.PYNATIVE_MODE)
    input = Tensor(np.random.random((1, 3, 224, 224)), mindspore.float32)
    vgg = vgg16()
    out = vgg(input)
    print(out.shape)
