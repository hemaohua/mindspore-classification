from mindspore import nn
import numpy as np


class AdaptDropout(nn.Dropout):
    def __init__(self, drop_rate=0.5):
        super(AdaptDropout, self).__init__(keep_prob=1 - drop_rate)


def make_divisible(x, divisor=4):
    return int(np.ceil(x * 1. / divisor) * divisor)
