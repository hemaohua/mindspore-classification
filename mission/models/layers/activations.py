import mindspore.nn as nn
import mindspore.ops as ops


class Mish(nn.Cell):
    """
    Mish activation method:

    $$Mish = x * tanh(ln(1 + e^x))$$
    """

    def __init__(self):
        super(Mish, self).__init__()
        self.mul = ops.Mul()
        self.tanh = ops.Tanh()
        self.soft_plus = ops.Softplus()

    def construct(self, x):
        """ mish act construct. """
        res1 = self.soft_plus(x)
        tanh = self.tanh(res1)
        output = self.mul(x, tanh)
        return output


class Swish(nn.Cell):
    """
    Swish activation function: x * sigmoid(x).
    """

    def __init__(self):
        super(Swish, self).__init__()
        self.sigmoid = ops.Sigmoid()

    def construct(self, x):
        """ construct swish """
        output = x * self.sigmoid(x)
        return output


