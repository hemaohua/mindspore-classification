from typing import Optional, Type, List, Union

import mindspore.nn as nn
from .layers.conv_norm_act import ConvNormActivation
from .layers.pooling import GlobalAvgPooling
from .utils import load_pretrained
from .registry import register_model
from mission.data.constants import IMAGENET_DEFAULT_MEAN, IMAGENET_DEFAULT_STD, DEFAULT_CROP_PCT

'''
1. 将model zoo里的 _conv3x3、_conv1x1、_conv7x7、_bn、_bn_last这些封装操作写法冗余，很乱，
直接用公用的ConvNormActivation就可以替换，能实现所有的功能

2. 关于初始化权重可以写个统一的_init_weights函数，放在里面统一执行。
3. 全局平均池化使用提出来公用的GlobalAvgPooling
'''

__all__ = ['ResNet', 'ResidualBlockBase', 'ResidualBlock']


def _cfg(url='', **kwargs):
    return {
        'url': url,
        'num_classes': 1000,
        'dataset_transform': {
            'transforms_imagenet_train': {
                'image_resize': 224,
                'scale': (0.08, 1.0),
                'ratio': (0.75, 1.333),
                'hflip': 0.5,
                'interpolation': 'bilinear',
                'mean': IMAGENET_DEFAULT_MEAN,
                'std': IMAGENET_DEFAULT_STD,
            },
            'transforms_imagenet_eval': {
                'image_resize': 224,
                'crop_pct': DEFAULT_CROP_PCT,
                'interpolation': 'bilinear',
                'mean': IMAGENET_DEFAULT_MEAN,
                'std': IMAGENET_DEFAULT_STD,
            },
        },
        'first_conv': 'conv1', 'classifier': 'fc',
        **kwargs
    }


default_cfgs = {
    'resnet18': _cfg(url=''),
    'resnet34': _cfg(url=''),
    'resnet50': _cfg(url=''),
    'resnet101': _cfg(url=''),
    'resnet152': _cfg(url=''),
    'resnext50_32x4d': _cfg(url=''),
    'resnext101_32x4d': _cfg(url=''),
    'resnext101_64x4d': _cfg(url=''),
    'resnext152_32x4d': _cfg(url=''),
}


class ResidualBlockBase(nn.Cell):
    expansion: int = 1

    # 先去掉了use_se和se_block参数，后面再实现SE-ResNet50部分，这里先不实现了，实现的有些乱，影响代码的可读性
    # 去掉了res_base参数，没有必要。
    # 添加了groups参数和base_width参数，用来实现ResNext网络
    # 添加了down_sample参数，主要是参考主流的写法

    def __init__(self,
                 in_channel: int,
                 out_channel: int,
                 stride: int = 1,
                 groups: int = 1,
                 base_width: int = 64,
                 norm: Optional[nn.Cell] = None,
                 down_sample: Optional[nn.Cell] = None
                 ) -> None:
        super(ResidualBlockBase, self).__init__()
        if not norm:
            norm = nn.BatchNorm2d
        assert groups != 1 or base_width == 64, "ResidualBlockBase only supports groups=1 and base_width=64"
        self.conv1 = ConvNormActivation(
            in_channel,
            out_channel,
            kernel_size=3,
            stride=stride,
            pad_mode='pad',
            norm=norm)
        self.conv2 = ConvNormActivation(
            out_channel,
            out_channel,
            kernel_size=3,
            norm=norm,
            pad_mode='pad',
            activation=None)
        self.relu = nn.ReLU()
        self.down_sample = down_sample

    def construct(self, x):
        """ResidualBlockBase construct."""
        identity = x

        out = self.conv1(x)
        out = self.conv2(out)

        if self.down_sample:
            identity = self.down_sample(x)
        out += identity
        out = self.relu(out)

        return out


class ResidualBlock(nn.Cell):
    expansion: int = 4

    # 先去掉了use_se和se_block参数，后面再实现SE-ResNet50部分，这里先不实现了，实现的有些乱，影响代码的可读性
    # 添加了groups参数和base_width参数，用来实现ResNext网络
    # 添加了down_sample参数，主要是参考主流的写法
    # BottleNeck的实现参考了torchvision的写法，比较主流和清晰，可以把resnet网络和resnext网络融合在一起
    def __init__(self,
                 in_channel: int,
                 out_channel: int,
                 stride: int = 1,
                 groups: int = 1,
                 base_width: int = 64,
                 norm: Optional[nn.Cell] = None,
                 down_sample: Optional[nn.Cell] = None
                 ) -> None:
        super(ResidualBlock, self).__init__()
        if not norm:
            norm = nn.BatchNorm2d

        out_channel = int(out_channel * (base_width / 64.0)) * groups

        self.conv1 = ConvNormActivation(
            in_channel, out_channel, kernel_size=1, norm=norm)
        self.conv2 = ConvNormActivation(
            out_channel,
            out_channel,
            kernel_size=3,
            stride=stride,
            pad_mode='pad',
            groups=groups,
            norm=norm)
        self.conv3 = ConvNormActivation(
            out_channel,
            out_channel *
            self.expansion,
            kernel_size=1,
            pad_mode='pad',
            norm=norm,
            activation=None)
        self.relu = nn.ReLU()
        self.down_sample = down_sample

    def construct(self, x):
        """ResidualBlock construct."""
        identity = x

        out = self.conv1(x)
        out = self.conv2(out)
        out = self.conv3(out)

        if self.down_sample:
            identity = self.down_sample(x)

        out += identity
        out = self.relu(out)

        return out


class ResNet(nn.Cell):

    def __init__(self,
                 block: Type[Union[ResidualBlockBase, ResidualBlock]],
                 layer_nums: List[int],
                 num_classes: int = 1000,
                 in_channels: int = 3,
                 groups: int = 1,
                 base_width: int = 64,
                 norm: Optional[nn.Cell] = None
                 ) -> None:
        super(ResNet, self).__init__()
        if not norm:
            norm = nn.BatchNorm2d
        self.norm = norm
        self.num_classes = num_classes
        self.in_channels = 64
        self.groups = groups
        self.base_with = base_width
        self.conv1 = ConvNormActivation(
            in_channels, self.in_channels, kernel_size=7, stride=2, pad_mode='pad', norm=norm)
        self.max_pool = nn.MaxPool2d(kernel_size=3, stride=2, pad_mode='same')
        self.layer1 = self._make_layer(block, 64, layer_nums[0])
        self.layer2 = self._make_layer(block, 128, layer_nums[1], stride=2)
        self.layer3 = self._make_layer(block, 256, layer_nums[2], stride=2)
        self.layer4 = self._make_layer(block, 512, layer_nums[3], stride=2)

        self.pool = GlobalAvgPooling()
        self.num_features = 512 * block.expansion
        self.classifier = nn.Dense(self.num_features, num_classes)

    def _make_layer(self,
                    block: Type[Union[ResidualBlockBase, ResidualBlock]],
                    channel: int,
                    block_nums: int,
                    stride: int = 1
                    ):
        """Block layers."""
        down_sample = None

        if stride != 1 or self.in_channels != self.in_channels * block.expansion:
            down_sample = ConvNormActivation(
                self.in_channels,
                channel * block.expansion,
                kernel_size=1,
                stride=stride,
                norm=self.norm,
                activation=None)
        layers = []
        layers.append(
            block(
                self.in_channels,
                channel,
                stride=stride,
                down_sample=down_sample,
                groups=self.group,
                base_width=self.base_with,
                norm=self.norm
            )
        )
        self.in_channels = channel * block.expansion

        for _ in range(1, block_nums):
            layers.append(
                block(
                    self.in_channels,
                    channel,
                    groups=self.group,
                    base_width=self.base_with,
                    norm=self.norm
                )
            )

        return nn.SequentialCell(layers)

    # 获取分类器的部分， 实现参考了Timmy的get_classifier的写法
    def get_classifier(self):
        return self.classifier

    # 修改分类器，参考了Timmy的写法
    def reset_classifier(self, num_classes):
        self.classifier = nn.Dense(self.num_features, num_classes)

    # 该函数主要是特征提取层，获取特征的，实现参考了Timmy的forwar_features的写法
    def get_features(self, x):
        x = self.conv1(x)
        x = self.max_pool(x)

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)

        return x

    def construct(self, x):
        x = self.get_features(x)
        x = self.pool(x)
        x = self.classifier(x)


@register_model
def resnet18(pretrained: bool = False, num_classes: int = 1000, in_channels=3, **kwargs):
    default_cfg = default_cfgs['resnet18']
    model = ResNet(ResidualBlockBase, [2, 2, 2, 2], num_classes=num_classes, in_channels=in_channels, **kwargs)
    model.dataset_transform = default_cfg['dataset_transform']

    if pretrained:
        load_pretrained(model, default_cfg, num_classes=num_classes, in_channels=in_channels)

    return model


@register_model
def resnet34(pretrained: bool = False, num_classes: int = 1000, in_channels=3, **kwargs):
    default_cfg = default_cfgs['resnet34']
    model = ResNet(ResidualBlockBase, [3, 4, 6, 3], num_classes=num_classes, in_channels=in_channels, **kwargs)
    model.dataset_transform = default_cfg['dataset_transform']

    if pretrained:
        load_pretrained(model, default_cfg, num_classes=num_classes, in_channels=in_channels)

    return model


@register_model
def resnet50(pretrained: bool = False, num_classes: int = 1000, in_channels=3, **kwargs):
    default_cfg = default_cfgs['resnet50']
    model = ResNet(ResidualBlock, [3, 4, 6, 3], num_classes=num_classes, in_channels=in_channels, **kwargs)
    model.dataset_transform = default_cfg['dataset_transform']

    if pretrained:
        load_pretrained(model, default_cfg, num_classes=num_classes, in_channels=in_channels)

    return model


@register_model
def resnet101(pretrained: bool = False, num_classes: int = 1000, in_channels=3, **kwargs):
    default_cfg = default_cfgs['resnet101']
    model = ResNet(ResidualBlock, [3, 4, 23, 3], num_classes=num_classes, in_channels=in_channels, **kwargs)
    model.dataset_transform = default_cfg['dataset_transform']

    if pretrained:
        load_pretrained(model, default_cfg, num_classes=num_classes, in_channels=in_channels)

    return model


@register_model
def resnet152(pretrained: bool = False, num_classes: int = 1000, in_channels=3, **kwargs):
    default_cfg = default_cfgs['resnet152']
    model = ResNet(ResidualBlock, [3, 8, 36, 3], num_classes=num_classes, in_channels=in_channels, **kwargs)
    model.dataset_transform = default_cfg['dataset_transform']

    if pretrained:
        load_pretrained(model, default_cfg, num_classes=num_classes, in_channels=in_channels)

    return model


@register_model
def resnet50_32x4d(pretrained: bool = False, num_classes: int = 1000, in_channels=3, **kwargs):
    default_cfg = default_cfgs['resnet50_32x4d']
    model = ResNet(ResidualBlock, [3, 4, 6, 3], groups=32, base_width=4, num_classes=num_classes,
                   in_channels=in_channels, **kwargs)
    model.dataset_transform = default_cfg['dataset_transform']

    if pretrained:
        load_pretrained(model, default_cfg, num_classes=num_classes, in_channels=in_channels)

    return model


@register_model
def resnet101_32x4d(pretrained: bool = False, num_classes: int = 1000, in_channels=3, **kwargs):
    default_cfg = default_cfgs['resnet101_32x4d']
    model = ResNet(ResidualBlock, [3, 4, 23, 3], groups=32, base_width=4, num_classes=num_classes,
                   in_channels=in_channels, **kwargs)
    model.dataset_transform = default_cfg['dataset_transform']

    if pretrained:
        load_pretrained(model, default_cfg, num_classes=num_classes, in_channels=in_channels)

    return model


@register_model
def resnet101_64x4d(pretrained: bool = False, num_classes: int = 1000, in_channels=3, **kwargs):
    default_cfg = default_cfgs['resnet101_64x4d']
    model = ResNet(ResidualBlock, [3, 4, 23, 3], groups=64, base_width=4, num_classes=num_classes,
                   in_channels=in_channels, **kwargs)
    model.dataset_transform = default_cfg['dataset_transform']

    if pretrained:
        load_pretrained(model, default_cfg, num_classes=num_classes, in_channels=in_channels)

    return model


@register_model
def resnet152_32x4d(pretrained: bool = False, num_classes: int = 1000, in_channels=3, **kwargs):
    default_cfg = default_cfgs['resnet152_32x4d']
    model = ResNet(ResidualBlock, [3, 8, 36, 3], groups=32, base_width=4, num_classes=num_classes,
                   in_channels=in_channels, **kwargs)
    model.dataset_transform = default_cfg['dataset_transform']

    if pretrained:
        load_pretrained(model, default_cfg, num_classes=num_classes, in_channels=in_channels)

    return model
