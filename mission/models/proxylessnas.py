# Copyright 2021 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""ProxylessNAS-Mobile model definition"""
from collections import OrderedDict
import mindspore.nn as nn
import mindspore.ops.operations as P
from .registry import register_model

# 删除get_activation_layer
# 删除ConvBlock：功能：conv2d+batchnorm+act
# 删除conv1x1_block和conv3x3_block


class ProxylessBlock(nn.Cell):
    """
    ProxylessBlock: the block for residual path in class ProxylessUnit.
    """
    def __init__(self, in_channels, out_channels, kernel_size, stride, bn_eps, expansion):
        super(ProxylessBlock, self).__init__()
        self.use_bc = (expansion > 1)
        mid_channels = in_channels * expansion

        if self.use_bc:
            self.bc_conv = nn.Conv2dBnAct(in_channels, mid_channels, 1, pad_mode="pad", has_bn=True, eps=bn_eps,
                                          activation='relu6')

        padding = (kernel_size - 1) // 2
        self.dw_conv = nn.Conv2dBnAct(mid_channels, mid_channels, kernel_size=kernel_size, stride=stride, pad_mode="pad",
                                      padding=padding, group=mid_channels, has_bn=True, eps=bn_eps, activation="relu6")

        self.pw_conv = nn.Conv2dBnAct(mid_channels, out_channels, 1, pad_mode="pad", has_bn=True, eps=bn_eps)

    def construct(self, x):
        if self.use_bc:
            x = self.bc_conv(x)
        x = self.dw_conv(x)
        x = self.pw_conv(x)
        return x


class ProxylessUnit(nn.Cell):
    def __init__(self, in_channels, out_channels, kernel_size, stride, bn_eps, expansion, residual, shortcut):
        super(ProxylessUnit, self).__init__()
        assert (residual or shortcut)
        self.residual = residual
        self.shortcut = shortcut

        if self.residual:
            self.body = ProxylessBlock(
                in_channels=in_channels,
                out_channels=out_channels,
                kernel_size=kernel_size,
                stride=stride,
                bn_eps=bn_eps,
                expansion=expansion)

    def construct(self, x):
        if not self.residual:
            return x
        if not self.shortcut:
            return self.body(x)
        identity = x
        x = self.body(x)
        x = identity + x
        return x


# 循环创建5个stage，之前每个stage都是一个一个写的
class ProxylessNAS(nn.Cell):
    """
    ProxylessNAS: Direct Neural Architecture Search on Target Task and Hardware
    https://arxiv.org/abs/1812.00332v1
    ProxylessNAS: the implementation of ProxylessNAS-Mobile model.
    """
    def __init__(self, channels, init_block_channels, final_block_channels, strides, residuals, shortcuts,
                 kernel_sizes, expansions, bn_eps=1e-3, in_channels=3, in_size=(224, 224), num_classes=1000):
        super(ProxylessNAS, self).__init__()
        self.in_size = in_size
        self.num_classes = num_classes

        stages = OrderedDict()
        stages["init"] = nn.Conv2dBnAct(in_channels, init_block_channels, 3, stride=2, pad_mode="pad", padding=1,
                                        has_bn=True, eps=bn_eps, activation="relu6")
        temp_in_channel = init_block_channels
        len_channels = len(channels)
        for i in range(len_channels):
            stage = OrderedDict()
            for j in range(len(channels[i])):
                stage[f"unit{j}"] = ProxylessUnit(
                    in_channels=temp_in_channel,
                    out_channels=channels[i][j],
                    kernel_size=kernel_sizes[i][j],
                    stride=strides[i][j],
                    bn_eps=bn_eps,
                    expansion=expansions[i][j],
                    residual=residuals[i][j],
                    shortcut=shortcuts[i][j])
                temp_in_channel = channels[i][j]
            stages[f"stage{i}"] = nn.SequentialCell(stage)

        stages[f"final_block"] = nn.Conv2dBnAct(temp_in_channel, final_block_channels, 1, pad_mode="pad", has_bn=True,
                                                eps=bn_eps, activation="relu6")
        stages[f"final_pool"] = nn.AvgPool2d(kernel_size=7, stride=1, pad_mode='valid')

        self.features = nn.SequentialCell(stages)
        self.output = nn.Dense(in_channels=final_block_channels, out_channels=num_classes)

    def construct(self, x):
        x = self.features(x)

        x = P.Reshape()(x, (P.Shape()(x)[0], -1,))
        x = self.output(x)
        return x

cfg = {
    "proxylessnax": "ckpt_url"
}

@register_model
def proxylessnas_mobile(num_classes=1000, pretrained=False):
    residuals = [[True], [True, True, False, False], [True, True, True, True], [True, True, True, True, True, True, True, True], [True, True, True, True, True]]
    channels = [[16], [32, 32, 32, 32], [40, 40, 40, 40], [80, 80, 80, 80, 96, 96, 96, 96],
                [192, 192, 192, 192, 320]]
    kernel_sizes = [[3], [5, 3, 3, 3], [7, 3, 5, 5], [7, 5, 5, 5, 5, 5, 5, 5], [7, 7, 7, 7, 7]]
    strides = [[1], [2, 1, 1, 1], [2, 1, 1, 1], [2, 1, 1, 1, 1, 1, 1, 1], [2, 1, 1, 1, 1]]
    expansions = [[1], [3, 3, 3, 3], [3, 3, 3, 3], [6, 3, 3, 3, 6, 3, 3, 3], [6, 6, 3, 3, 6]]
    init_block_channels = 32
    final_block_channels = 1280

    shortcuts = [[False], [False, True, True, True], [False, True, True, True], [False, True, True, True, False, True, True, True], [False, True, True, True, False]]

    net = ProxylessNAS(
        channels=channels,
        init_block_channels=init_block_channels,
        final_block_channels=final_block_channels,
        kernel_sizes=kernel_sizes,
        strides=strides,
        residuals=residuals,
        shortcuts=shortcuts,
        expansions=expansions,
        num_classes=num_classes
        )
    if pretrained:
        pass
    return net


if __name__ == '__main__':
    import mindspore
    from mindspore import Tensor, context
    import numpy as np
    context.set_context(mode=context.PYNATIVE_MODE)
    input = Tensor(np.random.random((1, 3, 224, 224)), mindspore.float32)
    proxy = proxylessnas_mobile()
    out = proxy(input)
    print(out.shape)