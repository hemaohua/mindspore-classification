# Copyright 2021 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""GhostNet model define"""
import math
import mindspore.nn as nn
from mindspore.ops import operations as P
from layers import make_divisible, AdaptDropout, SE
from mindspore.common.initializer import initializer, Normal
from .registry import register_model

__all__ = ['ghostnet_1x', 'ghostnet_nose_1x']


# 删除自定义的MyHSigmoid，用nn.HSigmoid代替

# 删除Activation

# 删除自定义的Conv2dBnAct，用nn.Conv2dBnAct代替

# 删除GlobalAvgPooling，因为它值封装了一个P.ReduceMean(),固用此代替


class GhostModule(nn.Cell):
    """
    GhostModule wrapper definition.

    Args:
        num_in (int): Input channel.
        num_out (int): Output channel.
        kernel_size (int): Input kernel size.
        stride (int): Stride size.
        padding (int): Padding number.
        ratio (int): Reduction ratio.
        dw_size (int): kernel size of cheap operation.
        act_type (string): Activation type.

    Returns:
        Tensor, output tensor.

    Examples:
        >>> GhostModule(3, 3)
    """
    # 删除use_act参数,是否使用act由act_type决定
    def __init__(self, num_in, num_out, kernel_size=1, stride=1, padding=0, ratio=2, dw_size=3,
                 act_type='relu'):
        super(GhostModule, self).__init__()
        init_channels = math.ceil(num_out / ratio)
        new_channels = init_channels * (ratio - 1)

        self.primary_conv = nn.Conv2dBnAct(num_in, init_channels, kernel_size=kernel_size, stride=stride,
                                           pad_mode="pad", padding=padding, group=1, has_bn=True, activation=act_type)

        self.cheap_operation = nn.Conv2dBnAct(init_channels, new_channels, kernel_size=dw_size, stride=1,
                                              pad_mode="pad", padding=dw_size // 2, group=init_channels, has_bn=True,
                                              activation=act_type)
        self.concat = P.Concat(axis=1)

    def construct(self, x):
        x1 = self.primary_conv(x)
        x2 = self.cheap_operation(x1)
        return self.concat((x1, x2))
    # 区别：pytorch还对输出的channel维做了切片，torch.cat([x1, x2], dim=1)[:, :self.oup, :, :]
    # self.oup对应num_out


# 用P.Add代替P.TensorAdd
# 删除_get_pad, 用(kernel_size - 1) // 2代替
class GhostBottleneck(nn.Cell):
    """
    GhostBottleneck wrapper definition.

    Args:
        num_in (int): Input channel.
        num_mid (int): Middle channel.
        num_out (int): Output channel.
        kernel_size (int): Input kernel size.
        stride (int): Stride size.
        act_type (str): Activation type.
        use_se (bool): Use SE warpper or not.

    Returns:
        Tensor, output tensor.

    Examples:
        >>> GhostBottleneck(16, 3, 1, 1)
    """

    def __init__(self, num_in, num_mid, num_out, kernel_size, stride=1, act_type='relu', use_se=False):
        super(GhostBottleneck, self).__init__()
        self.ghost1 = GhostModule(num_in, num_mid, kernel_size=1,
                                  stride=1, padding=0, act_type=act_type)

        self.use_dw = stride > 1
        self.dw = None
        if self.use_dw:
            self.dw = nn.Conv2dBnAct(num_mid, num_mid, kernel_size=kernel_size, stride=stride, pad_mode="pad",
                                     padding=(kernel_size - 1) // 2, group=num_mid, has_bn=True)
        self.use_se = use_se
        if use_se:
            self.se = SE(num_mid)

        self.ghost2 = GhostModule(num_mid, num_out, kernel_size=1, stride=1,
                                  padding=0, act_type=act_type)

        self.down_sample = (num_in != num_out or stride != 1)
        self.shortcut = None
        if self.down_sample:
            self.shortcut = nn.SequentialCell([
                nn.Conv2dBnAct(num_in, num_in, kernel_size=kernel_size, stride=stride, pad_mode="pad",
                               padding=(kernel_size - 1) // 2, group=num_in, has_bn=True),
                nn.Conv2dBnAct(num_in, num_out, kernel_size=1, pad_mode="pad", padding=0,
                               group=1, has_bn=True)
            ])
        self.add = P.Add()

    def construct(self, x):
        shortcut = x
        out = self.ghost1(x)
        if self.use_dw:
            out = self.dw(out)
        if self.use_se:
            out = self.se(out)
        out = self.ghost2(out)
        if self.down_sample:
            shortcut = self.shortcut(shortcut)
        out = self.add(shortcut, out)
        return out


# AdaptDropout代替Dropout
class GhostNet(nn.Cell):
    """
    GhostNet: More Features from Cheap Operations
    https://arxiv.org/abs/1911.11907
    GhostNet architecture.

    Args:
        model_cfgs (Cell): number of classes.
        num_classes (int): Output number classes.
        multiplier (int): Channels multiplier for round to 8/16 and others. Default is 1.
        drop_rate (float): probability of element to be zeroed.
    Returns:
        Tensor, output tensor.

    Examples:
        >>> GhostNet(num_classes=1000)
    """

    def __init__(self, model_cfgs, num_classes=1000, multiplier=1., drop_rate=0.5):
        super(GhostNet, self).__init__()
        self.cfgs = model_cfgs['cfg']
        out_channel = make_divisible(multiplier * 16)

        self.conv2dbnact = nn.Conv2dBnAct(3, out_channel, 3, pad_mode="pad", padding=1, stride=2,
                                          has_bn=True, activation="relu")
        in_channel = out_channel
        self.blocks = []
        for layer_cfg in self.cfgs:
            out_channel = make_divisible(multiplier * layer_cfg[2])
            self.blocks.append(GhostBottleneck(num_in=in_channel,
                                               num_mid=make_divisible(multiplier * layer_cfg[1]),
                                               num_out=out_channel,
                                               kernel_size=layer_cfg[0],
                                               stride=layer_cfg[5],
                                               act_type=layer_cfg[4],
                                               use_se=layer_cfg[3]
                                               ))
            in_channel = out_channel

        out_channel = make_divisible(multiplier * self.cfgs[-1][1])
        self.blocks.append(nn.Conv2dBnAct(in_channel, out_channel, 1, pad_mode="pad", activation="relu"))
        self.blocks = nn.SequentialCell(self.blocks)

        self.global_pool = P.ReduceMean(keep_dims=True)
        in_channel = out_channel
        out_channel = 1280
        self.conv_head = nn.Conv2d(in_channels=in_channel,
                                   out_channels=out_channel,
                                   kernel_size=1, padding=0, stride=1,
                                   has_bias=True, pad_mode='pad')
        self.act2 = nn.ReLU()
        self.squeeze = P.Flatten()
        self.drop_rate = drop_rate
        if self.drop_rate > 0:
            self.dropout = AdaptDropout(drop_rate)

        self.classifier = nn.Dense(out_channel, num_classes, has_bias=True)

        # pytorch 无参数初始化
        self._init_weights()

    def construct(self, x):
        x = self.conv2dbnact(x)
        x = self.blocks(x)
        x = self.global_pool(x, (2, 3))
        x = self.conv_head(x)
        x = self.act2(x)
        x = self.squeeze(x)
        if self.drop_rate > 0:
            x = self.dropout(x)
        x = self.classifier(x)
        return x

    # 用initializer替换之前Tensor(np.random.)初始化权重
    def _init_weights(self):
        self.init_parameters_data()
        for _, m in self.cells_and_names():
            if isinstance(m, (nn.Conv2d, )):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.set_data(initializer(Normal(0, np.sqrt(2. / n)),
                                              m.weight.data.shape))
                if m.bias is not None:
                    m.bias.set_data(initializer('zeros', m.bias.data.shape))
            elif isinstance(m, nn.BatchNorm2d):
                m.gamma.set_data(initializer('ones', m.gamma.data.shape))
                m.beta.set_data(initializer('zeros', m.beta.data.shape))
            elif isinstance(m, nn.Dense):
                m.weight.set_data(initializer('normal', m.weight.data.shape))
                if m.bias is not None:
                    m.bias.set_data(initializer('zeros', m.bias.data.shape))


model_cfgs = {
        "1x": {
            "cfg": [
                # k, exp, c,  se,     nl,  s,
                # stage1
                [3, 16, 16, False, 'relu', 1],
                # stage2
                [3, 48, 24, False, 'relu', 2],
                [3, 72, 24, False, 'relu', 1],
                # stage3
                [5, 72, 40, True, 'relu', 2],
                [5, 120, 40, True, 'relu', 1],
                # stage4
                [3, 240, 80, False, 'relu', 2],
                [3, 200, 80, False, 'relu', 1],
                [3, 184, 80, False, 'relu', 1],
                [3, 184, 80, False, 'relu', 1],
                [3, 480, 112, True, 'relu', 1],
                [3, 672, 112, True, 'relu', 1],
                # stage5
                [5, 672, 160, True, 'relu', 2],
                [5, 960, 160, False, 'relu', 1],
                [5, 960, 160, True, 'relu', 1],
                [5, 960, 160, False, 'relu', 1],
                [5, 960, 160, True, 'relu', 1]],
            "ckpt_url": "",
        },

        "nose_1x": {
            "cfg": [
                # k, exp, c,  se,     nl,  s,
                # stage1
                [3, 16, 16, False, 'relu', 1],
                # stage2
                [3, 48, 24, False, 'relu', 2],
                [3, 72, 24, False, 'relu', 1],
                # stage3
                [5, 72, 40, False, 'relu', 2],
                [5, 120, 40, False, 'relu', 1],
                # stage4
                [3, 240, 80, False, 'relu', 2],
                [3, 200, 80, False, 'relu', 1],
                [3, 184, 80, False, 'relu', 1],
                [3, 184, 80, False, 'relu', 1],
                [3, 480, 112, False, 'relu', 1],
                [3, 672, 112, False, 'relu', 1],
                # stage5
                [5, 672, 160, False, 'relu', 2],
                [5, 960, 160, False, 'relu', 1],
                [5, 960, 160, False, 'relu', 1],
                [5, 960, 160, False, 'relu', 1],
                [5, 960, 160, False, 'relu', 1]],
            "ckpt_url": "",
        }
    }


@register_model
def ghostnet_1x(pretrained=False, drop_rate=0.2, **kwargs):
    cfg = model_cfgs["1x"]
    net = GhostNet(cfg, drop_rate=drop_rate, **kwargs)
    if pretrained:
        pass
    return net


@register_model
def ghostnet_nose_1x(pretrained=False, drop_rate=0.2, **kwargs):
    cfg = model_cfgs["nose_1x"]
    net = GhostNet(cfg, drop_rate=drop_rate, **kwargs)
    if pretrained:
        pass
    return net

# pytorch还有ghostnet_050和ghostnet_130两个规格，
# 即将multiplier设置为0.5和1.3


if __name__ == '__main__':
    import mindspore
    from mindspore import Tensor, context
    import numpy as np
    context.set_context(mode=context.PYNATIVE_MODE)
    input = Tensor(np.random.random((1, 3, 224, 224)), mindspore.float32)
    ghostnet = ghostnet_1x()
    out = ghostnet(input)
    print(out.shape)